<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        // dd($posts);
        return view('articles', [
            'posts' => $posts
        ]);
    }

    public function show($id)
    {
        $posts = [
            '1' => 'titre 1',
            '2' => 'titre 2'
        ];

        $post = $posts[$id] ?? 'pas de titre';

        return view('article', [
            'post' => $post
        ]);
    }

    public function contact()
    {
        return view('contact');
    }



}
